import pytest

from silkaj.tx import (
    truncBase,
    display_pubkey,
    display_amount,
    display_output_and_amount,
)
from silkaj.money import UDValue


# truncBase()
@pytest.mark.parametrize(
    "amount,base,expected",
    [(0, 0, 0), (10, 2, 0), (100, 2, 100), (306, 2, 300), (3060, 3, 3000)],
)
def test_truncBase(amount, base, expected):
    assert truncBase(amount, base) == expected


# display_amount()
@pytest.mark.parametrize("message, amount, currency_symbol", [("Total", 1000, "Ğ1")])
@pytest.mark.asyncio
async def test_display_amount(message, amount, currency_symbol):
    amount_UD = round(amount / await UDValue().ud_value, 4)
    expected = [
        [
            message + " (unit | relative)",
            str(amount / 100)
            + " "
            + currency_symbol
            + "  ||  "
            + str(amount_UD)
            + " UD",
        ]
    ]
    tx = list()
    await display_amount(tx, message, amount, currency_symbol)
    assert tx == expected


# display_pubkey()
@pytest.mark.parametrize(
    "message, pubkey, id",
    [
        ("From", "CmFKubyqbmJWbhyH2eEPVSSs4H4NeXGDfrETzEnRFtPd", "Matograine"),
        ("To", "9qJjmuYi4Sti89Lm81G7AoFo5QLpmxd1NSMqN4Z1BwhS", ""),
    ],
)
@pytest.mark.asyncio
async def test_display_pubkey(message, pubkey, id):
    if id == "":
        expected = [[message + " (pubkey)", pubkey]]
    else:
        expected = [[message + " (pubkey)", pubkey], [message + " (id)", id]]
    tx = list()
    await display_pubkey(tx, message, pubkey)
    print("DEBUG : tx : ", tx)  # to see if display_pubkey gets the ID
    assert tx == expected


# display_output_and_amount()
@pytest.mark.parametrize(
    "outputAddresses, amount, currency_symbol, ids",
    [
        (["CmFKubyqbmJWbhyH2eEPVSSs4H4NeXGDfrETzEnRFtPd"], 1000, "Ğ1", ["Matograine"]),
        (["d88fPFbDdJXJANHH7hedFMaRyGcnVZj9c5cDaE76LRN"], 1000, "Ğ1", [""]),
        (
            [
                "CmFKubyqbmJWbhyH2eEPVSSs4H4NeXGDfrETzEnRFtPd",
                "d88fPFbDdJXJANHH7hedFMaRyGcnVZj9c5cDaE76LRN",
            ],
            1000,
            "Ğ1",
            ["Matograine", ""],
        ),
    ],
)
@pytest.mark.asyncio
async def test_display_output_and_amount(
    outputAddresses, amount, currency_symbol, ids
):
    amount_UD = round(amount / await UDValue().ud_value, 4)
    expected = list()
    if len(outputAddresses) == 1:
        if ids(0) == "":
            expected.append(
                ["to (pubkey)", outputAddresses[0]],
                [
                    "amount (unit | relative)",
                    str(amount / 100)
                    + " "
                    + currency_symbol
                    + "  ||  "
                    + str(amount_UD)
                    + " UD",
                ],
            )
        else:
            expected.append(
                ["to (pubkey)", outputAddresses],
                ["to (id)", ids(0)],
                [
                    "amount (unit | relative)",
                    str(amount / 100)
                    + " "
                    + currency_symbol
                    + "  ||  "
                    + str(amount_UD)
                    + " UD",
                ],
            )
    else:
        for outputAddress, id in zip(outputAddresses, ids):
            if id == "":
                expected.append(
                    ["to (pubkey)", outputAddress],
                    [
                        "amount (unit | relative)",
                        str(amount / 100)
                        + " "
                        + currency_symbol
                        + "  ||  "
                        + str(amount_UD)
                        + " UD",
                    ],
                )
            else:
                expected.append(
                    ["to (pubkey)", outputAddress],
                    ["to (id)", id],
                    [
                        "amount (unit | relative)",
                        str(amount / 100)
                        + " "
                        + currency_symbol
                        + "  ||  "
                        + str(amount_UD)
                        + " UD",
                    ],
                )
    tx = list()
    await display_output_and_amount(tx, outputAddresses, amount, currency_symbol)
    assert tx == expected
